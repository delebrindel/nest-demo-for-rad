import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  require('dotenv').config();
}

(async()=>{
  const port = process.env.PORT || 3100;
  const onLoadMessage = process.env.ON_LOAD_MESSAGE || 'Holi';
  const app = await NestFactory.create(AppModule);
  await app.listen(port, () => {
    console.log(` Service ${process.env.npm_package_version} listening on http://localhost:${port}\n ${onLoadMessage}`);
  });
})();
