import { Body, Controller, Get, HttpCode, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { User } from './types/user.types';

/*
& Add a user
POST http://localhost:3100 HTTP/1.2
content-type: application/json

{
  "name": "Iannis",
  "age": 32
}

& List all users
GET http://localhost:3100 HTTP/1.2
content-type: application/json
*/
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getUserList(): User[] {
    return this.appService.getUsers();
  }

  @Post()
  @HttpCode(200)
  async addUser(@Body() userData: User): Promise<User[]>  {
    await this.appService.addUser(userData);
    return this.appService.getUsers();
  }
}
