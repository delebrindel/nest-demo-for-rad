import { Injectable } from '@nestjs/common';
import { User } from './types/user.types';

@Injectable()
export class AppService {
  private users = [];

  getUsers(): User[] {
    return this.users;
  }

  addUser(userData: User): void{
    this.users.push(userData)
  }
}
