import { Body, Controller, Get, HttpCode, Param, Post, Query } from '@nestjs/common';

type quesoRequest = {
  nombre: string;
  calidad: string;
  cantidad?: number;
}

@Controller('user')
export class UserController {
  @Get('galleta')
  dameGalleta(@Query() miQuery){
    const nombre = miQuery.nombre ?? 'None';
    const sabor = miQuery.sabor ?? 'Vainilla';
    return `Hola ${nombre}. Ten galleta de ${sabor}`;
  }

  @Get('galleta/:cuantas')
  dameGalletas(@Param('cuantas') cuantasGalletas, @Query() miQuery){
    const nombre = miQuery.nombre ?? 'None';
    const sabor = miQuery.sabor ?? 'Vainilla';
    return `Hola ${nombre}. Ten ${cuantasGalletas} galletas de ${sabor}.`;
  }

  @Post('queso')
  @HttpCode(200)
  dameQueso(@Body() miBody: quesoRequest){
    const nombre = miBody.nombre;
    const calidad = miBody.calidad;
    const cantidad = miBody.cantidad;
    return `Hola, ten ${cantidad} queso(s) ${nombre} de calidad ${calidad}.`;
  }
}
